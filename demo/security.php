<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="security ct">
                    <ul class="security-link sub-nav">
                        <li class="link Personal Info"><a href="setting<?=$_sub?>">Personal Info</a></li>
                        <li class="active inactive link Security"><a href="security<?=$_sub?>">Security</a></li>
                        <li class="inactive link Verification"><a href="verification<?=$_sub?>">Verification</a></li>
                    </ul>

                    <h2 class="lineTitle"><b>Mobile Phone</b></h2>
                    <div class="securityTxt">
                        <h3 class="h3-text">Mobile phone number verification</h3>
                        <p class="text">Enter your mobile phone number to get the verification SMS message.</p>
                        
                        <form class="form">
                            <div class="display-table formList phoneList">
                                <dl>
                                    <dt><p class="text">Phone</p></dt>
                                    <dd>
                                        <select class="selector" name="countryCode">
                                            <option value="886" label="886 (Taiwan)"></option>
                                        </select>
                                         <input name="phone" class="field" label="phone" type="text" placeholder="Please enter your phone number" value="">
                                    </dd>
                                    <dd>
                                        <button class="btn btn-submit" type="submit" name="button">Send</button>
                                    </dd>
                                    
                                </dl>
                            </div>
                        </form>
                    </div>



                    <h2 class="lineTitle"><b>Security</b></h2>

                    <div class="securityTxt">

                        <h3 class="h3-text">Dual identity verification</h3>
                        <p class="text">Use Google Authenticator to protect your login and transaction security.</p>

                        <hr>
                        
                        <h3 class="h3-text">Currently protecting the project</h3>
                        <ul class="security-stepList inlineBlock">
                            <li>
                                <div>
                                    <i class="fas fa-sign-in-alt"></i>
                                    <p>Sign in</p>
                                </div>
                            </li>                            

                            <li>
                                <div>
                                    <i class="fas fa-check-double"></i>
                                    <p>Pick up confirmation</p>
                                </div>
                            </li>                            

                            <li>
                                <div>
                                    <i class="fas fa-key"></i>
                                    <p>Change the password</p>
                                </div>
                            </li>

                        </ul> 

                        <hr>                  

                        <h3 class="h3-text">Ways of identifying</h3>
                        <p class="text">Google Authenticator certification</p>
                        <!--<div class="qr"><img src="img/QR.png"></div>-->
                    </div>

                    <hr>
                    <button class="btn btn-submit FR">Reset</button>

                    <div class="modal hidden">
                        <section class="modal-section">
                            <div class="otp-verify">
                                <h2 class="h2-text">QRCode</h2>
                                <img class="img" alt=""src="img/QR.png">
                                <form class="form">
                                    <div class="display-table formList">
                                        <dl><dt><p class="text">OTP verify</p></dt>
                                            <dd>
                                                <input name="otpKey" class="field" label="otpKey" type="text" value="">
                                            </dd>
                                        </dl>
                                    </div>
                                    <hr>
                                    <button class="btn btn-submit" type="submit" name="button">Verify</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </section>
                    </div>

                </div>
            </main>
            <?php include("include/footer.html"); ?>


        </div>
    </div>
    
</body>

</html>