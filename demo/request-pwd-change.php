<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>
            <main>
                <div class="request-pwd-change">
                     <h2 class="lineTitle"><b>Forget Password</b></h2>
                     <form class="form">
                        <p class="text">Please go to your Email to check the reset token after input your email address and send.</p>
                        <div class="display-table formList">
                            <dl>
                                <dt><p class="text">Email</p></dt>
                                <dd><input name="email" class="field" label="email" type="email" value=""></dd>
                            </dl>
                         </div>
                         <hr>
                        <button class="btn btn-submit" type="submit" name="button">Send</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>

</html>