<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="signup">
                    <h2 class="lineTitle"><b>Register</b></h2>
                    <form class="form">

                        <div class="display-table formList">
                            <dl>
                                <dt><p class="text">Email</p></dt>
                                <dd><input name="email" class="field" label="email" type="email" value=""></dd>
                            </dl>
                            
                            <dl>
                                <dt><p class="text"> Password </p></dt>
                                <dd>
                                    <input name="pwd" class="field" label="pwd" type="password" value="">
                                </dd>
                             </dl>                            

                             <dl>
                                <dt><p class="text">Check Password </p></dt>
                                <dd>
                                    <input name="pwd" class="field" label="pwd" type="password" value="">
                                </dd>
                             </dl>

                         </div>

                         <hr>
                        
                        <button class="btn btn-submit" type="submit" name="button">Sign Up</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </main>
            <?php include("include/footer.html"); ?>
        </div>
    </div>
    
</body>

</html>