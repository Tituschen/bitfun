<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="personalInfo  ct">
                    <ul class="personalInfo-link sub-nav">
                        <li class="active link Personal Info"><a href="setting<?=$_sub?>">Personal Info</a></li>
                        <li class="inactive link Security"><a href="security<?=$_sub?>">Security</a></li>
                        <li class="inactive link Verification"><a href="verification<?=$_sub?>">Verification</a></li>
                    </ul>

                    <h2 class="lineTitle"><b>Account modify</b></h2>
                    
                    <div class="personalinfo-modifier">

                        <div class="modifyList display-table">
                            <dl>
                                <dt>Name</dt>
                                <dd class="modify-txt">Titus Chen</dd>
                                <dd class="modify-btn"><button class="btn btn-modify">Modify</button></dd>
                            </dl>                       

                             <dl>
                                <dt>Password</dt>
                                <dd class="modify-txt">*********</dd>
                                <dd class="modify-btn"><button class="btn btn-modify">Modify</button></dd>
                            </dl>                         

                            <dl>
                                <dt>Email</dt>
                                <dd class="modify-txt">a.a@a.com</dd>
                                <dd class="modify-btn"><button class="btn btn-modify">Modify</button></dd>
                            </dl>                        

                            <dl>
                                <dt>Mobile Phone</dt>
                                <dd class="modify-txt">09xx-123-xxx</dd>
                                <dd class="modify-btn"><button class="btn btn-modify">Modify</button></dd>
                            </dl>                        

                            <dl>
                                <dt>Birth</dt>
                                <dd class="modify-txt">1980 / 04 / 20</dd>
                                <dd class="modify-btn"><button class="btn btn-modify">Modify</button></dd>
                            </dl>

                        </div>
                        
                        <div class="modifier">
                            <h3 class="h3-text">Modify Phone</h3>
                            <div class="phone-form">
                                <form class="form">
                                    <div class="display-table formList">
                                        <dl><dt><p class="text">Phone Number</p></dt>
                                            <dd>
                                                <input name="phoneNumber" class="field" label="phoneNumber" type="number" placeholder="Please enter your phone number" value="">
                                            </dd>
                                        </dl>
                                    </div>
                                    <hr>
                                    <button class="btn btn-submit" type="submit" name="button">Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <?php include("include/footer.html"); ?>


        </div>
    </div>
    
</body>

</html>