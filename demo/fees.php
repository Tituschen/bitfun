<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="fees ctW">
                    <h3 class="text">Order fees and limitations</h3>
                    <ul class="orderfee">
                        <li class="orderfee-title">
                            <span class="pair">Pair</span>
                            <span class="minamount">Minimum Order Amount</span>
                            <span class="mindigit">Minimum Order Number Of Digit</span>
                            <span class="makerfee">Maker Fees</span>
                            <span class="takerfee">Taker Fees</span>
                        </li>
                        <li class="orderfee-data"><span>BTC/USD</span><span>4</span><span>0.0001 BTC</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>ETH/USD</span><span>3</span><span>0.005 ETH</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>NXC/USD</span><span>2</span><span>0.01 NXC</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>BTC/TWD</span><span>4</span><span>0.0025 BTC</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>ETH/TWD</span><span>3</span><span>0.005 ETH</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>NXC/TWD</span><span>2</span><span>0.01 NXC</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>ETH/BTC</span><span>2</span><span>0.16 ETH</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>NXC/BTC</span><span>2</span><span>0.01 NXC</span><span>1%</span><span>1%</span></li>
                        <li class="orderfee-data"><span>NXC/ETH</span><span>2</span><span>0.01 NXC</span><span>1%</span><span>1%</span></li>
                    </ul>
                    <h3 class="text">withdrawal-fees</h3>
                    <ul class="withdrawalfee">
                        <li class="withdrawalfee-title">
                            <span class="currency">Currency</span>
                            <span class="fee">Fee</span>
                            <span class="minamount">Minimum Trading Amount</span>
                            <span class="maxamount">Maximum Trading Amount</span>
                            <span class="dailyamount">Daily Cumulative Maximum Amount</span>
                        </li>
                        <li class="withdrawalfee-data"><span>BTC</span><span>0.0002</span><span>0.001</span><span>50</span><span>100</span></li>
                        <li class="withdrawalfee-data"><span>ETH</span><span>0.002</span><span>0.005</span><span>1000</span><span>2000</span></li>
                        <li class="withdrawalfee-data"><span>NXC</span><span>15</span><span>100</span><span>1000000</span><span>2000000</span></li>
                        <li class="withdrawalfee-data"><span>USD</span><span>3</span><span>20</span><span>100000</span><span>200000</span></li>
                        <li class="withdrawalfee-data"><span>TWD</span><span>15</span><span>100</span><span>1000000</span><span>2000000</span></li>
                    </ul>
                    <h3 class="text">The cryptocurrency deposit fee and blockchain confirmation required for crediting</h3>
                    <ul class="cryptofee">
                        <li class="cryptofee-title">
                            <span class="currency">Currency</span>
                            <span class="fees">General Deposit Fees</span>
                            <span class="blockchain-required">Blockchain Confirmation Required</span>
                        </li>
                        <li class="cryptofee-data"><span>BTC</span><span>0</span><span>4</span></li>
                        <li class="cryptofee-data"><span>ETH</span><span>0</span><span>20</span></li>
                        <li class="cryptofee-data"><span>NXC</span><span>0</span><span>1</span></li>
                    </ul>
                </div>
            </main>
            <?php include("include/footer.html"); ?>
        </div>
    </div>
    
</body>

</html>