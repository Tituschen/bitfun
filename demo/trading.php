<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="trading">

                    <section class="trading-L">
                        <article>
                            <ul class="primary-currency">
                                <li class="link favor"><i class="fas fa-star"></i></li>
                                <li class="active link USD">USD</li>
                                <li class="link TWD">TWD</li>
                                <li class="link BTC">BTC</li>
                                <li class="link ETH">ETH</li>
                            </ul>

                            <div class="quote">
                                <ul class="quote-list">
                                    <li class="list-title">
                                        <span class="currency">Currency</span>
                                        <span class="price">Price</span>
                                        <span class="favorite">Favorite</span>
                                    </li>                                    
                                    <li>
                                        <span class="currency">ETH</span>
                                        <span class="price">10</span>
                                        <span class="favorite"><i class="fas fa-star"></i></span>
                                    </li>                                    
                                    <li class="favor">
                                        <span class="currency">ETH</span>
                                        <span class="price">10</span>
                                        <span class="favorite"><i class="fas fa-star"></i></span>
                                    </li>
                                    <li>
                                        <span class="currency">ETH</span>
                                        <span class="price">10</span>
                                        <span class="favorite"><i class="fas fa-star"></i></span>
                                    </li>                                    
                                    <li class="favor">
                                        <span class="currency">ETH</span>
                                        <span class="price">10</span>
                                        <span class="favorite"><i class="fas fa-star"></i></span>
                                    </li>
                                    
                                </ul>
                            </div>

                            <div class="Bids">
                                <h3 class="title">Bid ETH / BTC</h3>

                                <div class="btns">
                                    <button class="btn active grainentBtn">Buy</button>
                                    <button class="btn grainentBtn">Sell</button>

                                    <div class="clearAll"></div>
                                </div>

                                <div class="buy-form">
                                    <h3 class="h3-text">Order Type</h3>
                                    <form class="form">
                                        <select class="selector" name="rule" type="number">
                                            <option value="2" label="Limit"></option>
                                            <option value="1" label="Market"></option>
                                        </select>
                                        <h3 class="h3-text">Price BTC</h3>
                                        <input name="price" class="field" label="price" type="text" values="0" value="0">
                                        <h3 class="h3-text">Amount ETH</h3>
                                        <h4 class="h4-text">(Limit Amount : 0.16)</h4>
                                        <p class="text">Available Balance 22 BTC</p>
                                        
                                        <div class="amountArea">
                                            <select class="selector" name="percent" type="number">
                                                <option value="" label="Select percent"></option>
                                                <option value="0.25" label="25%"></option>
                                                <option value="0.50" label="50%"></option>
                                                <option value="0.75" label="75%"></option>
                                                <option value="1" label="100%"></option>
                                            </select>
                                            <input name="amount" class="field" label="amount" type="text" value="0">
                                        </div>

                                        <h4 class="total">Total: <b>0 BTC</b></h4>
                                        <button class="btn btn-trading grainentBtn btn-buy" type="submit" name="button">Buy</button>
                                    </form>
                                </div>

                            </div>

                            <div class="trading-asset">
                                <ul class="list  quote-list">
                                    <li class="list-title"><span class="currency">Currency</span><span class="balance">Balance</span></li>
                                    <li class="list-data"><span class="currency">USD</span><span class="balance">200000000</span></li>
                                    <li class="list-data"><span class="currency">TWD</span><span class="balance">20000000000</span></li>
                                    <li class="list-data"><span class="currency">NXC</span><span class="balance">200000000</span></li>
                                    <li class="list-data"><span class="currency">BTC</span><span class="balance">22</span></li>
                                    <li class="list-data"><span class="currency">ETH</span><span class="balance">100.01</span></li>
                                </ul>
                            </div>


                        </article>

                    </section>

                    <section class="trading-C">
                         <article>

                        <div id="tvContainer" class="trading-tradingview">
                            <iframe id="tradingview_74ef0" name="tradingview_74ef0" src="/includes/charting_library/static/en-tv-chart.e6c523133ea801233691.html#symbol=ETH%2FBTC&amp;interval=180&amp;widgetbar=%7B%22details%22%3Afalse%2C%22watchlist%22%3Afalse%2C%22watchlist_settings%22%3A%7B%22default_symbols%22%3A%5B%5D%7D%7D&amp;timeFrames=%5B%7B%22text%22%3A%225y%22%2C%22resolution%22%3A%22W%22%7D%2C%7B%22text%22%3A%221y%22%2C%22resolution%22%3A%22W%22%7D%2C%7B%22text%22%3A%226m%22%2C%22resolution%22%3A%22120%22%7D%2C%7B%22text%22%3A%223m%22%2C%22resolution%22%3A%2260%22%7D%2C%7B%22text%22%3A%221m%22%2C%22resolution%22%3A%2230%22%7D%2C%7B%22text%22%3A%225d%22%2C%22resolution%22%3A%225%22%7D%2C%7B%22text%22%3A%221d%22%2C%22resolution%22%3A%221%22%7D%5D&amp;locale=en&amp;uid=tradingview_74ef0&amp;clientId=0&amp;userId=0&amp;chartsStorageVer=1.0&amp;debug=false&amp;timezone=Asia%2FTaipei&amp;theme=dark" frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen="" style="display: block; width: 100%; height: 100%;"></iframe>
                        </div>

                        <div class="orderbook">
                                <h3 class="title">Order Book (ETH / BTC)</h3>
                                <div class="buyorder">
                                    <ul class="buyorder-list">
                                        <li class="list-title">
                                            <span class="order">Order</span>
                                            <span class="amount">Amount</span>
                                            <span class="total">Total</span>
                                            <span class="price">Price</span>
                                        </li>
                                        <li class="list-data">
                                            <div class="left-green">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">3</span><span class="total">3</span><span class="price">15</span></div>
                                                <div class="bar" style="width:7%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="left-green">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">10</span><span class="total">13</span><span class="price">10</span></div>
                                                 <div class="bar" style="width:11%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="left-green">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">10</span><span class="total">23</span><span class="price">3</span></div>
                                                 <div class="bar" style="width:19%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="left-green 100">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">100</span><span class="total">123</span><span class="price">0.03</span></div>
                                                 <div class="bar" style="width:100%">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="sellorder">
                                    <ul class="sellorder-list">
                                        <li class="list-title">
                                            <span class="price">Price</span>
                                            <span class="total">Total</span>
                                            <span class="amount">Amount</span>
                                            <span class="order">Order</span>
                                        </li>
                                        <li class="list-data">
                                            <div class="right-red">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">50</span><span class="total">50</span><span class="price">200</span></div>
                                                 <div class="bar" style="width:50%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="right-red ">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">20</span><span class="total">70</span><span class="price">300</span></div>
                                                 <div class="bar" style="width:70%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="right-red">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">29</span><span class="total">99</span><span class="price">399</span></div>
                                                 <div class="bar" style="width:99%">
                                            </div>
                                        </li>
                                        <li class="list-data">
                                            <div class="right-red">
                                                <div class="text"><span class="order-amount">1</span><span class="amount">0.99</span><span class="total">99.99</span><span class="price">1000</span></div>
                                                 <div class="bar" style="width:100%">
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="clearAll"></div>
                            </div>

                        <div class="records">

                            <h4 class="title-dobuleline"><b>Open Orders</b></h4>
                            <table class="openorder-table">
                                <thead>
                                    <tr class="table-header">
                                        <th class="col col-date" scope="col">Date</th>
                                        <th class="col col-type" scope="col">Type</th>
                                        <th class="col col-price" scope="col">Price</th>
                                        <th class="col col-amount" scope="col">Amount</th>
                                        <th class="col col-filled" scope="col">Filled</th>
                                        <th class="col col-filled" scope="col">Total (BTC)</th>
                                        <th class="col col-status" scope="col">Status</th>
                                        <th class="col col-action" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:38:8</td>
                                        <td>Limit Buy</td>
                                        <td>10</td>
                                        <td>10</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:38:26</td>
                                        <td>Limit Buy</td>
                                        <td>3</td>
                                        <td>10</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:39:19</td>
                                        <td>Limit Buy</td>
                                        <td>15</td>
                                        <td>3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:39:37</td>
                                        <td>Limit Buy</td>
                                        <td>0.03</td>
                                        <td>100</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:42:54</td>
                                        <td>Limit Sell</td>
                                        <td>200</td>
                                        <td>50</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:17</td>
                                        <td>Limit Sell</td>
                                        <td>300</td>
                                        <td>20</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>Limit Sell</td>
                                        <td>1000</td>
                                        <td>0.99</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:45:36</td>
                                        <td>Limit Sell</td>
                                        <td>399</td>
                                        <td>29</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>Unfilled</td>
                                        <td>
                                            <button>Cancel</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <div class="cancel-form">
                                <div class="message"> Are You Sure To Cancel This Order?</div>
                                <button class="btn btn-submit">Yes</button>
                                <button class="btn cancel-btn">No</button>
                            </div>

                            <h4 class="title-dobuleline"><b>Filled Orders</b></h4>
                            <table class="fillorder-table">
                                <thead>
                                    <tr class="table-header">
                                        <th class="col col-date" scope="col">Date</th>
                                        <th class="col col-type" scope="col">Type</th>
                                        <th class="col col-price" scope="col">Price</th>
                                        <th class="col col-amount" scope="col">Amount</th>
                                        <th class="col col-filledP" scope="col">Filled Price</th>
                                        <th class="col col-filled" scope="col">Filled</th>
                                        <th class="col col-total" scope="col">Total (BTC)</th>
                                        <th class="col col-status" scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>SELL</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>SELL</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    <tr class="table-body">
                                        <td>2019/11/10 20:43:56</td>
                                        <td>limited Price</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>fullfille</td>
                                        <td>BUY</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>

                       </article>
                    </section>

                    <section class="trading-R">

                         <article>
                                <div class="trader">
                                    <h3 class="title">Trades (ETH / BTC)</h3>
                                    <table class="table">
                                        <thead>
                                            <tr class="table-header">
                                                <th class="col col-time" scope="col">Time</th>
                                                <th class="col col-price" scope="col">Price</th>
                                                <th class="col col-amount" scope="col">Amount</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                        

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                            

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                        

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                           

                                             <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                        

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                            

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>                                        

                                            <tr class="table-body">
                                                <td>21:09:20</td>
                                                <td>3,088</td>
                                                <td>0.0678</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                        </article>
                    </section>

                </div>
            </main>
            <?php include("include/footer.html"); ?>
        </div>
    </div>
    
</body>

</html>