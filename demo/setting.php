<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="personalInfo  ct">
                    <ul class="personalInfo-link sub-nav">
                        <li class="active link Personal Info"><a href="setting<?=$_sub?>">Personal Info</a></li>
                        <li class="inactive link Security"><a href="security<?=$_sub?>">Security</a></li>
                        <li class="inactive link Verification"><a href="verification<?=$_sub?>">Verification</a></li>
                    </ul>

                    <h2 class="lineTitle"><b>Account</b></h2>
                    <div class="personal-info-form">
                        <form class="form">

                             <div class="display-table formList">
                                <dl>
                                    <dt><p class="text">First Name</p></dt>
                                    <dd> 
                                        <input name="firstname" class="field" label="firstname" type="text" placeholder="Please enter your First Name" value="">
                                        <div class="message">Invalid firstname</div>
                                    </dd>
                                </dl>
                            
                                <dl>
                                    <dt><p class="text">Last Name</p></dt>
                                    <dd>
                                        <input name="lastname" class="field" label="lastname" type="text" placeholder="Please enter your Last Name" value="">
                                    </dd>
                                 </dl>                                

                                 <dl>
                                    <dt><p class="text">Address</p></dt>
                                    <dd> <input name="address" class="field" label="address" type="text" placeholder="Please enter your address" value=""></dd>
                                </dl>
                            
                                <dl>
                                    <dt><p class="text">Birth</p></dt>
                                    <dd>
                                        <input name="birth" class="field" label="birth" type="date" placeholder="Please enter your birthdate" value="">
                                    </dd>
                                 </dl>                               

                                 <dl>
                                    <dt><p class="text">Phone</p></dt>
                                    <dd>
                                        <input name="phone" class="field" label="phone" type="number" placeholder="Please enter your phone number" value="">
                                    </dd>
                                 </dl>

                            </div>
                            <hr>
                            <button class="btn btn-submit" type="submit" name="button">Send</button>
                        </form>

                    </div>
                </div>
            </main>
            <?php include("include/footer.html"); ?>


        </div>
    </div>
    
</body>

</html>