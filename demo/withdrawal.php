<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>
            <main>
                <div class="deposit ctW">
                    <ul class="assets-link sub-nav">
                        <li class="link Assets"><a href="assets<?=$_sub?>">Assets</a></li>
                        <li class="link Deposit"><a href="deposit<?=$_sub?>">Deposit</a></li>
                        <li class="active  link Withdrawal"><a href="withdrawal<?=$_sub?>">Withdrawal</a></li>
                        <li class="link Order History"><a href="orderhistory<?=$_sub?>">Order History</a></li>
                        <li class="link Currency History"><a href="currencyhistory<?=$_sub?>">Currency History</a></li>
                        <li class="link Bonus History"><a href="bonushistory<?=$_sub?>">Bonus History</a></li>
                    </ul>

                    <div class="row">

                        <div class="col-sm-6">

                            <form class="deposit-form">
                                <h2><b>Currency</b></h2>
                                <select class="selector" name="currency">
                                    <option value="USD" label="USD - Unites States Dollar"></option>
                                    <option value="TWD" label="TWD - Taiwan Dollar"></option>
                                    <option value="BTC" label="BTC - BitCoin"></option>
                                    <option value="ETH" label="ETH - Ethereum"></option>
                                    <option value="NXC" label="NXC - Next"></option>
                                </select>

                                 <p class="text">Withdrawal to</p>
                                <select class="selector" name="bank">
                                    <option value="" label="Select Bank"></option>
                                    <option value="aetheras" label="Aetheras Bank (last five digits : 12345)"></option>
                                    <option value="taiwan" label="Taiwan bank (last five digits : 45678)"></option>
                                    <option value="ABC" label="America Banana Bank - (last five digits : 7777)"></option>
                                </select>
                                <p class="text">Amount</p>
                                <input name="amount" class="field blackInput" label="amount" type="number" min="0" placeholder="minimum withdrawal amount is 100 TWD" value="0">
                                
                                 <div class="display-table dealList">
                                    <dl>
                                        <dt>Fee-</dt>
                                        <dd>-1000 twd</dd>
                                    </dl>                                    
                                    <dl>
                                        <dt>Total Amount-</dt>
                                        <dd> -1000 twd</dd>
                                    </dl>
                                 </div>           
   
                                <button class="btn btn-submit" type="submit" name="button">Withdrawal</button>

                            </form>

                        </div>
                        
                        <div class="col-sm-6">
                            <h2><b>Notice</b></h2>
                            <ol class="noticeList">
                                <li>The minimum withdrawal amount is 100 TWD</li>
                                <li>The maximum withdrawal amount is 1,000,000 TWD</li>
                                <li>Higher Daily Withdrawal is 2,000,000 TWD</li>
                                <li>Service fee is 15 TWD</li>
                                <li>Withdrawal before 14:30 will transfer on the same day. If request at non-banking hours or holidays will transfer on the next working day.</li>
                            </ol>

                        </div>

                    </div>
                    
                    <h4 class="title-dobuleline"><b>Recent withdrawal details</b></h4>
                    <table class="table">
                        <thead>
                            <tr class="table-header">
                                <th class="col col-time" scope="col">Time</th>
                                <th class="col col-transactionNo" scope="col">Transaction No.</th>
                                <th class="col col-accountNo" scope="col">Account No.</th>
                                <th class="col col-amount" scope="col">Amount</th>
                                <th class="col col-status" scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>                            
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>
                        </tbody>
                    </table>

                    <a class="FR" href="currencyHistory<?=$_sub?>">
                        <button class="btn btnBlue">View withdraw history</button>
                    </a>
                    </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>

</html>