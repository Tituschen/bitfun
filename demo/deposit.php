<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>
            <main>
                <div class="deposit ctW">
                    <ul class="assets-link sub-nav">
                        <li class="link Assets"><a href="assets<?=$_sub?>">Assets</a></li>
                        <li class="active  link Deposit"><a href="deposit<?=$_sub?>">Deposit</a></li>
                        <li class="link Withdrawal"><a href="withdrawal<?=$_sub?>">Withdrawal</a></li>
                        <li class="link Order History"><a href="orderhistory<?=$_sub?>">Order History</a></li>
                        <li class="link Currency History"><a href="currencyhistory<?=$_sub?>">Currency History</a></li>
                        <li class="link Bonus History"><a href="bonushistory<?=$_sub?>">Bonus History</a></li>
                    </ul>

                    <div class="row">

                        <div class="col-sm-6">

                        <form class="deposit-form">
                            <h2><b>Currency</b></h2>
                            <select class="selector MB10" name="currency">
                                <option value="USD" label="USD - Unites States Dollar"></option>
                                <option value="TWD" label="TWD - Taiwan Dollar"></option>
                                <option value="BTC" label="BTC - BitCoin"></option>
                                <option value="ETH" label="ETH - Ethereum"></option>
                                <option value="NXC" label="NXC - Next"></option>
                            </select>
                        </form>

                        <ol class="infoList">
                            <li>Transfer fund to the exclusive bank account to proceed deposit transaction</li>
                            <li>Bank: 遠東商業銀行(805)</li>
                            <li>Branch: 0012 營業部</li>
                            <li>Account Name: 英屬維京群島商幣託科技有限公司台灣分公司</li>
                            <li>Account No.: 1020000416809189</li>
                        </ol>

                        </div>
                        
                        <div class="col-sm-6">
                            <h2><b>Notice</b></h2>
                            <ol class="noticeList">
                                <li>Transfer fund must use the verified bank account. Using unverified bank account to transfer, the order will be failed. If the order is failed, user will be required to provide the certificate of remittance for the refund purpose. It will be only returned back to the transferred account, and will be charged related remittance fee.
                                </li>
                                <li>Currently we don't accept remitting money over bank counter service. Please kindly use ATM or Web ATM for bank wire.</li>
                                <li>You could transfer more than TWD 30,000 after pre-defined your own exclusive account with your bank.</li>
                                <li>Depositing TWD will take around 5 to 15 minutes, this does not apply if an error occurs within the banking systems.</li>
                            </ol>

                        </div>

                    </div>
                    
                    <h4 class="title-dobuleline"><b>Recent deposit details</b></h4>
                    <table class="table">
                        <thead>
                            <tr class="table-header">
                                <th class="col col-time" scope="col">Time</th>
                                <th class="col col-transactionNo" scope="col">Transaction No.</th>
                                <th class="col col-account" scope="col">Account No.</th>
                                <th class="col col-amount" scope="col">Amount</th>
                                <th class="col col-status" scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>                            
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>
                        </tbody>
                    </table>

                    <a class="FR" href="currencyHistory<?=$_sub?>">
                        <button class="btn btnBlue">View deposit history</button>
                    </a>
                    </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>

</html>
