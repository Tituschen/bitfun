<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>
            <main>
                <div class="assets ctW">
                    <ul class="assets-link sub-nav">
                        <li class="active link Assets"><a href="assets<?=$_sub?>">Assets</a></li>
                        <li class="inactive link Deposit"><a href="deposit<?=$_sub?>">Deposit</a></li>
                        <li class="inactive link Withdrawal"><a href="withdrawal<?=$_sub?>">Withdrawal</a></li>
                        <li class="inactive link Order History"><a href="orderhistory<?=$_sub?>">Order History</a></li>
                        <li class="inactive link Currency History"><a href="currencyhistory<?=$_sub?>">Currency History</a></li>
                        <li class="inactive link Bonus History"><a href="bonushistory<?=$_sub?>">Bonus History</a></li>
                    </ul>

                    <div class="account-amount flex-center">
                        <div>
                            <h2><small>$</small>500222224444</h2>
                            <h5>Total Assests<br>(TWD)</h5>
                        </div>
                    </div>

                    <button class="btn btnBlue MB10">Hide zero balance</button>

                    <table class="table">
                        <thead>
                            <tr class="table-header">
                                <th class="col col-currency" scope="col">Currency</th>
                                <th class="col col-totalAmount" scope="col">Total Amount</th>
                                <th class="col col-available" scope="col">Available Balance</th>
                                <th class="col col-funds" cope="col">Funds</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-body">
                                <td>BTC</td>
                                <td>0.000005</td>
                                <td>0.000005</td>
                                <td>
                                    <button class="btn">Deposit</button>
                                    <button class="btn">Withdrawal</button>
                                </td>
                            </tr>
                            <tr class="table-body">
                                <td>ETH</td>
                                <td>0.000005</td>
                                <td>0.000005</td>
                                <td>
                                    <button class="btn">Deposit</button>
                                    <button class="btn">Withdrawal</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>

</html>