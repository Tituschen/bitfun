<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="currency-record ctW">
                    <ul class="assets-link sub-nav">
                        <li class="link Assets"><a href="assets<?=$_sub?>">Assets</a></li>
                        <li class="link Deposit"><a href="deposit<?=$_sub?>">Deposit</a></li>
                        <li class="link Withdrawal"><a href="withdrawal<?=$_sub?>">Withdrawal</a></li>
                        <li class="link Order History"><a href="orderhistory<?=$_sub?>">Order History</a></li>
                        <li class="active  link Currency History"><a href="currencyhistory<?=$_sub?>">Currency History</a></li>
                        <li class="link Bonus History"><a href="bonushistory<?=$_sub?>">Bonus History</a></li>
                    </ul>

                     <form class="currency-record-form">
                        <div class="row searchRow">
                            <div class="col-sm-6 col-md-3">

                                <p class="text">Currency</p>
                                <select class="selector" name="currency">
                                    <option value="USD" label="USD"></option>
                                    <option value="TWD" label="TWD"></option>
                                    <option value="BTC" label="BTC"></option>
                                    <option value="ETH" label="ETH"></option>
                                    <option value="NXC" label="NXC"></option>
                                </select>

                            </div>                                

                            <div class="col-sm-6 col-md-3">
                                <p class="text">Type</p>
                                <select class="selector" name="type">
                                    <option value="All" label="All"></option>
                                    <option value="Credit" label="Deposit"></option>
                                    <option value="Debit" label="Withdrawal"></option>
                                </select>
                            </div>                                

                            <div class="col-sm-12 col-md-6">
                                <p class="text">Search Duration</p>
                                <div class="display-table calendarList">
                                    <dt>
                                        <input name="sourceDate" class="field blackInput" label="sourceDate" type="Date" value="">
                                        <div class="message">Please enter a date</div>
                                    </dt>
                                    <dd class="c1">TO</dd>                                 
                                    <dt>
                                        <input name="targetDate" class="field blackInput" label="targetDate" type="Date" value="">
                                        <div class="message">Please enter a date</div>
                                    </dt>
                                    <dd class="c2"><button class="btn btn-submit" type="submit" name="button">Search</button></dd>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    
                    <h4 class="title-dobuleline"><b>Search result</b></h4>
                    <table class="table">
                        <thead>
                            <tr class="table-header">
                                <th class="col col-time" scope="col">Time</th>
                                <th class="col col-transcationNo" scope="col">Transaction No.</th>
                                <th class="col col-type" scope="col">Type</th>
                                <th class="col col-amount" scope="col">Amount</th>
                                <th class="col col-fee" scope="col">Fee</th>
                                <th class="col col-subtotal" scope="col">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>From testutils</td>
                                <td>Deposit</td>
                                <td>500</td>
                                <td>30</td>
                                <td>470</td>
                            </tr>                            

                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>From testutils</td>
                                <td>Deposit</td>
                                <td>500</td>
                                <td>30</td>
                                <td>470</td>
                            </tr>                            

                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>From testutils</td>
                                <td>Deposit</td>
                                <td>500</td>
                                <td>30</td>
                                <td>470</td>
                            </tr>                            

                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>From testutils</td>
                                <td>Deposit</td>
                                <td>500</td>
                                <td>30</td>
                                <td>470</td>
                            </tr>
                        </tbody>
                    </table>

                    </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>
</html>

