<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>

            <main>
                <div class="verification ct">
                    <ul class="security-link sub-nav">
                        <li class="link Personal Info"><a href="setting<?=$_sub?>">Personal Info</a></li>
                        <li class="link Security"><a href="security<?=$_sub?>">Security</a></li>
                        <li class="active link Verification"><a href="verification<?=$_sub?>">Verification</a></li>
                    </ul>

                    <h2 class="lineTitle">
                        <b>Level1</b>
                        <small>Digital Currency Trading</small>
                    </h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Security Verification</h4>
                            <button class="btn " disabled="">Verified</button>
                        </div>                        

                        <div class="col-sm-6">
                            <h4>Personal Information</h4>
                            <button class="btn ">Setup</button>
                        </div>

                    </div>

                    <h2 class="lineTitle">
                        <b>Level2</b>
                        <small>Fiat Trading</small>
                    </h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Identity Verification</h4>
                            <button class="btn hidden">Setup</button>
                                <div class="identity-form">
                                    <form class="form">
                                        <h3 class="h3-text">Identity photo </h3>
                                        <p class="text">Notice: </p>
                                        <p class="text">Input file must be less than 20MB and the file must be JPG, PNG or JPEG </p>
                                        <div class="display-table formList">
                                            <dl>
                                                <dd>
                                                    <input class="input" name="file" label="file" type="file">
                                                </dd>
                                            </dl>
                                        </div>
                                        <hr>
                                        <button class="btn btn-submit" type="submit" name="button">Send</button>
                                    </form>
                                </div>
                        </div>                        

                        <div class="col-sm-6">
                            <h4>Financial Verification</h4>
                            <button class="btn">Setup</button>
                        </div>

                    </div>

                </div>
            </main>
            <?php include("include/footer.html"); ?>


        </div>
    </div>
    
</body>

</html>