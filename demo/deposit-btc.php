<!DOCTYPE html>
<html lang="en">

<head>
<?php include("include/meta.php"); ?>
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <div class="layout">
            
			<?php include("include/header.html"); ?>
            <main>
                <div class="deposit ctW">
                    <ul class="assets-link sub-nav">
                        <li class="link Assets"><a href="assets<?=$_sub?>">Assets</a></li>
                        <li class="active  link Deposit"><a href="deposit<?=$_sub?>">Deposit</a></li>
                        <li class="link Withdrawal"><a href="withdrawal<?=$_sub?>">Withdrawal</a></li>
                        <li class="link Order History"><a href="orderhistory<?=$_sub?>">Order History</a></li>
                        <li class="link Currency History"><a href="currencyhistory<?=$_sub?>">Currency History</a></li>
                        <li class="link Bonus History"><a href="bonushistory<?=$_sub?>">Bonus History</a></li>
                    </ul>

                    <div class="row">

                        <div class="col-sm-6">
                            <form class="deposit-form">
                                <h2><b>Currency</b></h2>
                                <select class="selector MB10" name="currency">
                                    <option value="USD" label="USD - Unites States Dollar"></option>
                                    <option value="TWD" label="TWD - Taiwan Dollar"></option>
                                    <option value="BTC" label="BTC - BitCoin"></option>
                                    <option value="ETH" label="ETH - Ethereum"></option>
                                    <option value="NXC" label="NXC - Next"></option>
                                </select>
                            </form>

                                <p>Send BTC to below wallet address for deposit</p>

                                <div class="display-table deposit-copy">
                                    <dt>
                                        <input id="address" class="blackInput" readonly="" value="13hKXeZjSB5FcmuoS4Lm8Yy8VxQbevd3eQ">
                                    </dt>

                                    <dd>
                                    <span class="copy-btn">
                                        <button class="btn">COPY</button>
                                    </span>
                                    </dd>
                                </div>
                                
                                <img class="img qrcode" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAACUCAYAAAB1PADUAAAIg0lEQVR4Xu2dQZLjOAwE1/9/dG/M3ixHKCOnQMrNrbmCBMFCEoRlj/r18/Pz80//VYEhBV4FakjJuvlPgQJVEEYVKFCjctZZgSoDowoUqFE566xAlYFRBQrUqJx1VqDKwKgCBWpUzjorUGVgVIECNSpnnRWoMjCqQIEalbPOClQZGFWgQI3KWWcxUK/Xa6uK9PMtG8/V3/R869+Onxaf9KX1CtTl94UFKvu9ZYEqUG9FpxXqUoOnKwyVeLqiyH71b8dTfNb+dUClAV0FuAJi/dv5FkgLBAFj7RaYaX0//KW/KbcJswKk/u38AvVlPZStIASYBSI9gQWqQN0yaYEsUF8OlE2Q7SHIf+rPzqeKa+10IGj/aY+n413dQ63eMPm3QNgETl/x9sqm/RcoeC5EgKQCFqheebdVmQCcttsrgsZbwMmf3S/52/7YYHVJJsHpCrEVLR1PCbIJX60vxVug4MtsSmiBukds/Ls8mxCbIPJPTTKdaPJv47UnnNYnO61H80k/8l+gLgqR4AWqFepNAdtz2R6MgCN/BDTZqYLQ/FaoS09EghSo98cCqR5tyiWAVFFsRSB/toJQT0gVs0DJB582gQRImkAbDyU8jYf8kx7bK5QNaLXgdCWm8dr4ab3phE/HV6A2v6M2BSKdvxvY5Y8NaENkJ0FTO62f2ik+8p/O3+2/QJHioT0FIp1P4U/7HweKNpDa009Bv31+qh/NT3vMAgWPEegE77YTEKm9QP0yINIKmQJD8wtUgSJGlP1xoFS0CwbTgz1bEexzmvTKWyDJoy7jHurR6P/8bZHw9010IlNgaP7T+k2vX6DgQScBkdqnE/q0vwJVoEYZjIGyJzSNnq6o1P/q+ekVnca3Wr8ClWZIzi9QIFgrlCOqQBUoR4zU6zqcHnOkwfz6K2/1cx0SmBJEduufxq+u6HY/04At76EK1LsCBUqWcCLeCmpPnL1Cpv23QhEBBepWASufPVAEqD1ANN6u93EDpa/zsVeaDZgSYP3RePoURvNtwmi9FFgbj91fgZIVNxWYgChQMiEkKCWsFcq9r2kaUMpPK5Q8EFZQe8VMAzDtz+7/6x4b0AaoYj1ttz2ljZf0IaBpPet/vELZAO341QmixwbWvjpem/A0frteK5T8r+32QNB4a7cJLlCyx7GC2QRa/61Q6cewEAA6cdRk0nxrJ4Cm/aXykz52P3E80w827QmlBJFgNN/abQLIP/mLExj+pp6aeNrfeFNOC9IVY+fT+NROAFj/5K9ASUUL1OI3xp1WoehKohNKfO4+wRQP7ZeuEDuf4pleL9Y77aFIoAJ1X6EsMDT+cb0L1Ox3ZdMVgwCaXq8VSj62oARQAqkik387n+KZXu9xoOgxgRWExlNJJ7v1b/dH69uEWQCtf9LD2uOvXqzgNkB7AimhtD4lhBJM65P/VE/rn/Sw9gJ1UYwSUqDuEStQBcoWodvx40BRyaYrga448k/q0PpkX+0/rZCpfrQ+7b9AyTfgkaAEpLVPHyDyV6Dgqwc6sfTVEPVM0/4poWk8BUq+v2l1BVnt/38PFJ0YKvHTJ5z8ERBkp4r2bfNtfih+ssc9lA04HZ8CSoKQvUAtfmyQAkIVhRJo1ydgyE7xfNv87fp825fDVIHIToBSwsleoDZXKGoq008ZlND0RNr5FE8KKPm38ab6436mK1SByn4OQxWY7LZCE7AE0AegBepdEnvi04TQfGsvUPKPSu8WmBJE8dAJp/nWTvEed+VNVwASnBJKAtv5dCXZlsCuT3qQ3a73+JVXoFyPZRNMwJDdrlegQDECngRvhQprsCWeEmYTYtcnICg+mm/jJ3/WTnqQ3a73eIWigKc3TIB8OwBWL9uUk39rH/8uLyx4H3//btofCT4NNCUkXc8eGIontRco+X6oVHD6VGkPUIGSTbEVmBLWCjV9JN79La9QdILs9qjnscCk4yl+OhDT+tj9UHy0v+1N+bRgBcqlmPQqUNDz2BOajqf0UsKmD5zdD8VH+2uFGn6fEglOCStQpOBmO33sJvvqcC0wBCB96LBX3LQ+cVO+OiHknwQhO/lP7QUqVXDzfAKG7KvDLVCrFR72T8CQfTicD3cFSipsBZPuP4bbHoN6DvpURPOpZ7H7JX+p3e7Xxh/3UAVq9h2aKTA0v0BdFGiFcq+ppgOf6rn8OZQtkXZ8KkAqMPVk5J/2SxUmtf+6CpUmPO1ZaH1KOCVsNxC03vR+SD+M5+n/RoUByvc3kSDTCaATbysaxU8HzsZD4yk/y688KwgFPJ2QAvW6lTzN3/invDQgOoF0JdH6BeqXA0UJpJJrKxRVPAKO5tt4aD2rD8VHB470Jv9kX16hrGAkCNlpw5Rgml+g7hUqUETQxV6gCtSbAq1Q70Ckemz/lLf7yrMC7Y6PCmIa/+MtwernULsTlibEJtxegdY/jX96/VYo6IkogVQByG790/gCBQpRQqzdJoTG2/XTCk3xFKjFQNFzFkpAmkCaT3YLoN0vjaf4yH78YwNbUVAw+cY98vfRg8g/NUKAEKC256T9FCj40x+U8PGEFKj374aoIhDhNJ/s9sRaINIrk/ZPFYXmp/qQf7Ivr1AUANnTBNJ8SiABR/7TCmfjo3jITvkge4GS/5N4GhALbFqBC1TYBJOAtgIUqPsa1QrVCkW3mLKPA6VW/4vBadNJFYsqkA2Z4rVXHvmjK9HGb8cXqOG/GEoJTYEuUBZxGJ8KmibUbofibYW6KEpNrU0AjacEra4QFN/q9dP92/jt+PjKswt2/NkKFKiz87t9dwVqu+RnL1igzs7v9t0VqO2Sn71ggTo7v9t3V6C2S372ggXq7Pxu312B2i752QsWqLPzu313BWq75GcvWKDOzu/23RWo7ZKfvWCBOju/23dXoLZLfvaCBers/G7f3b8TxML5DITlqgAAAABJRU5ErkJggg==">
                            </div>
                        
                        <div class="col-sm-6">
                            <h2><b>Notice</b></h2>
                            <ol class="noticeList">
                                <li>Transfer fund must use the verified bank account. Using unverified bank account to transfer, the order will be failed. If the order is failed, user will be required to provide the certificate of remittance for the refund purpose. It will be only returned back to the transferred account, and will be charged related remittance fee.
                                </li>
                                <li>Currently we don't accept remitting money over bank counter service. Please kindly use ATM or Web ATM for bank wire.</li>
                                <li>You could transfer more than TWD 30,000 after pre-defined your own exclusive account with your bank.</li>
                                <li>Depositing TWD will take around 5 to 15 minutes, this does not apply if an error occurs within the banking systems.</li>
                            </ol>

                        </div>

                    </div>
                    
                    <h4 class="title-dobuleline"><b>Recent deposit details</b></h4>
                    <table class="table">
                        <thead>
                            <tr class="table-header">
                                <th class="col col-time" scope="col">Time</th>
                                <th class="col col-transactionNo" scope="col">Transaction No.</th>
                                <th class="col col-account" scope="col">Account No.</th>
                                <th class="col col-amount" scope="col">Amount</th>
                                <th class="col col-status" scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>                            
                            <tr class="table-body">
                                <td>2019/9/28 21:9:9</td>
                                <td>NAN</td>
                                <td>From testutils</td>
                                <td>500</td>
                                <td>NAN</td>
                            </tr>
                        </tbody>
                    </table>

                    <a class="FR" href="currencyHistory<?=$_sub?>">
                        <button class="btn btnBlue">View deposit history</button>
                    </a>
                    </div>
            </main>
            <?php include("include/footer.html"); ?>

        </div>
    </div>
    
</body>

</html>
